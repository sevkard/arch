#!/bin/bash
echo "Setting locale,console font, timezone and locale"
sed -i 's/#en_US.UTF-8/en_US.UTF-8/' /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" > /etc/locale.conf
echo "KEYMAP=la-latin1" > /etc/vconsole.conf
echo "FONT=ter-v16n" >> /etc/vconsole.conf
hwclock --systohc
ln -sf /usr/share/zoneinfo/America/Tijuana /etc/localtime
echo "sienna" >> /etc/hostname 
echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1 localhost" >> /etc/hosts
echo "127.0.1.1 sienna" >> /etc/hosts
