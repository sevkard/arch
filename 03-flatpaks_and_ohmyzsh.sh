#!/bin/bash

echo "Installing Flatpaks"

flatpak install -y com.dropbox.Client \
com.github.wwmm.easyeffects \
com.google.Chrome \
org.gnu.emacs

echo "Installing ohmyzsh"
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
