#!/bin/bash

echo "Installing apps"

pacman -S --needed --noconfirm accerciser \
amd-ucode \
base \
base-devel \
cheese \
dconf-editor \
endeavour \
eog \
evolution \
file-roller \
gdm \
geary \
ghex \
git \
glade \
gnome-backgrounds \
gnome-boxes \
gnome-builder \
gnome-calculator \
gnome-calendar \
gnome-characters \
gnome-clocks \
gnome-connections \
gnome-console \
gnome-contacts \
gnome-devel-docs \
gnome-dictionary \
gnome-font-viewer \
gnome-games \
gnome-logs \
gnome-maps \
gnome-multi-writer \
gnome-music \
gnome-notes \
gnome-photos \
gnome-recipes \
gnome-shell-extension-appindicator \
gnome-shell-extensions \
gnome-software \
gnome-sound-recorder \
gnome-system-monitor \
gnome-text-editor \
gnome-themes-extra \
gnome-tour \
gnome-tweaks \
gnome-user-docs \
gnome-weather \
grub \
linux
linux-firmware
loupe \
nautilus \
plymouth \
polari \
reflector \
snapshot \
sushi \
terminus-font \
totem \
ttf-fira-code \
vim \
xdg-user-dirs-gtk \
xf86-video-ati \
zsh

echo "Installing grub"
grub-install --target=x86_64-efi --efi-directory=/boot/efi
grub-mkconfig -o /boot/grub/grub.cfg

echo "Adding user and enabling services"

systemctl enable gdm
systemctl enable NetworkManager
reflector --country 'United States' -p http -n5 -l5 --sort rate --save /etc/pacman.d/mirrorlist
useradd -mG wheel -s /bin/zsh chon
passwd
passwd chon
